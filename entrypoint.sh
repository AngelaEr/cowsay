#!/bin/bash
if [ $# -eq 1 ]
then
     echo "########################################"
     echo "recived , PORT=$1"
     echo "########################################"
     export PORT=$1
else
     echo "########################################"
     echo "didnt receive, default PORT=8080"
     echo "########################################"
fi

echo "########################################"
echo "starting server"
echo "########################################"

npm start