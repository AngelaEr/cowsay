FROM node:16

WORKDIR /app

COPY src ./
RUN npm install 

COPY entrypoint.sh ./
RUN chmod +x entrypoint.sh

ENTRYPOINT [ "./entrypoint.sh" ]

# FROM alpine

# WORKDIR /app
# COPY . .
# RUN apk add --update npm
# RUN apk add --update nodejs nodejs-npm
# RUN apk add --no-cache python2
# RUN npm cache clean --force
# RUN npm install 
# RUN npm run build 

# RUN pip2 install -r requirements.txt

# CMD ["python2","app.py"]
